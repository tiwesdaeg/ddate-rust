extern crate chrono;
extern crate clap;
use chrono::{Datelike, Utc};
use clap::App;

fn main() {

    App::new("ddate")
        .version("0.1")
        .about("Converts Gregorian date to Discordian date")
        .author("Brett Hogan")
//  test
//        .arg(Arg::with_name("INPUT")
//            .short("d")
//            .long("date")
//            .help("Enter a date to convert YYYYMMDD (ex: -d 19970812)")
//            .required(true)
//            .index(1))
        .get_matches();
    let now = Utc::now();
    let dyear = now.year() + 1166;
    let year = now.year();
    let dy = now.ordinal();
    let ly = is_leap(year);
    let dyf = dayofyear(ly, dy);
    let s = seasonnum(dyf);
    let sd = seasonday(dyf);
    let (wd, wdb) = wholyday(s, sd);
//    println!("Using input file: {}", matches.value_of("INPUT").unwrap());

    if wdb {
        println!("{}, {} {}, in the YOLD {}. Celebrate {}!",
            weekday(dyf),
            season(dyf),
            seasonday(dyf),
            dyear,
            wd
        );
    } else {
        println!("{}, {} {}, in the YOLD {}",
            weekday(dy),
            season(dyf),
            seasonday(dy),
            dyear
        );
    }
}

fn wholyday(s: u32, sd: u32) -> (String, bool) {
    let hday: [String; 5] = [
        String::from("Mungday"),
        String::from("Mojoday"),
        String::from("Syaday"),
        String::from("Zaraday"),
        String::from("Maladay")
        ];
    let hflux: [String; 5] = [
        String::from("Chaoflux"),
        String::from("Discoflux"),
        String::from("Confuflux"),
        String::from("Bureflux"),
        String::from("Afflux")
        ];
    let hmas: [String; 5] = [
        String::from("Chaomas"),
        String::from("Discomas"),
        String::from("Confumas"),
        String::from("Buremas"),
        String::from("Afmas")
        ];
    let hsloth: [String; 5] = [
        String::from("Chaosloth"),
        String::from("Discosloth"),
        String::from("Confusloth"),
        String::from("Buresloth"),
        String::from("Afsloth")
        ];
    let heye: [String; 5] = [
        String::from("Mungeye"),
        String::from("Mojeye"),
        String::from("Syadeye"),
        String::from("Zareye"),
        String::from("Maleye")
        ];
    if s == 1 && sd == 5 {
        let bool_val = true;
        let wday = &hday[0];
        return (wday.to_string(), bool_val);
    } else if s == 2 && sd == 5 {
        let bool_val = true;
        let wday = &hday[1];
        return (wday.to_string(), bool_val);
    } else if s == 3 && sd == 5 {
        let bool_val = true;
        let wday = &hday[2];
        return (wday.to_string(), bool_val);
    } else if s == 4 && sd == 5 {
        let bool_val = true;
        let wday = &hday[3];
        return (wday.to_string(), bool_val);
    } else if s == 5 && sd == 5 {
        let bool_val = true;
        let wday = &hday[4];
        return (wday.to_string(), bool_val);
    } else if s == 1 && sd == 50 {
        let bool_val = true;
        let wday = &hflux[0];
        return (wday.to_string(), bool_val);
    } else if s == 2 && sd == 50 {
        let bool_val = true;
        let wday = &hflux[1];
        return (wday.to_string(), bool_val);
    } else if s == 3 && sd == 50 {
        let bool_val = true;
        let wday = &hflux[2];
        return (wday.to_string(), bool_val);
    } else if s == 4 && sd == 50 {
        let bool_val = true;
        let wday = &hflux[3];
        return (wday.to_string(), bool_val);
    } else if s == 5 && sd == 50 {
        let bool_val = true;
        let wday = &hflux[4];
        return (wday.to_string(), bool_val);
    } else if s == 1 && sd == 23 {
        let bool_val = true;
        let wday = &hmas[0];
        return (wday.to_string(), bool_val);
    } else if s == 2 && sd == 23 {
        let bool_val = true;
        let wday = &hmas[1];
        return (wday.to_string(), bool_val);
    } else if s == 3 && sd == 23 {
        let bool_val = true;
        let wday = &hmas[2];
        return (wday.to_string(), bool_val);
    } else if s == 4 && sd == 23 {
        let bool_val = true;
        let wday = &hmas[3];
        return (wday.to_string(), bool_val);
    } else if s == 5 && sd == 23 {
        let bool_val = true;
        let wday = &hmas[4];
        return (wday.to_string(), bool_val);
    } else if s == 1 && sd == 27 {
        let bool_val = true;
        let wday = &hsloth[0];
        return (wday.to_string(), bool_val);
    } else if s == 2 && sd == 27 {
        let bool_val = true;
        let wday = &hsloth[1];
        return (wday.to_string(), bool_val);
    } else if s == 3 && sd == 27 {
        let bool_val = true;
        let wday = &hsloth[2];
        return (wday.to_string(), bool_val);
    } else if s == 4 && sd == 27 {
        let bool_val = true;
        let wday = &hsloth[3];
        return (wday.to_string(), bool_val);
    } else if s == 5 && sd == 27 {
        let bool_val = true;
        let wday = &hsloth[4];
        return (wday.to_string(), bool_val);
    } else if s == 1 && sd == 73 {
        let bool_val = true;
        let wday = &heye[0];
        return (wday.to_string(), bool_val);
    } else if s == 2 && sd == 73 {
        let bool_val = true;
        let wday = &heye[1];
        return (wday.to_string(), bool_val);
    } else if s == 3 && sd == 73 {
        let bool_val = true;
        let wday = &heye[2];
        return (wday.to_string(), bool_val);
    } else if s == 4 && sd == 73 {
        let bool_val = true;
        let wday = &heye[3];
        return (wday.to_string(), bool_val);
    } else if s == 5 && sd == 73 {
        let bool_val = true;
        let wday = &heye[4];
        return (wday.to_string(), bool_val);

    } else {
        let bool_val = false;
        let wday = String::from(" ");
        return (wday, bool_val);
    }
}

// Determines leap year
fn is_leap(year: i32) -> bool {
    let factor = |x| year % x == 0;
    factor(4) && (!factor(100) || factor(400))
}

// Takes the year and corrects day of year on leap years
fn dayofyear(year: bool, day: u32) -> u32 {
    if year {
        if day < 60 {
            let cday = day;
            return cday;
        } else if day == 60 {
            return 366;
        } else {
            let cday = day - 1;
            return cday;
        }
    } else {
        return day;
    }
}

// Determines discordian Season based on day of dayofyear
fn season(day: u32) -> String {
    if day <= 73 {
        let season = String::from("Chaos");
        return season;
    } else if day == 366 {
        let season = String::from("Chaos");
        return season;
    } else if day >= 74 && day < 147 {
        let season = String::from("Discord");
        return season;
    } else if day >= 147 && day < 220 {
        let season = String::from("Confusion");
        return season;
    } else if day >= 220 && day < 293 {
        let season = String::from("Bureaucracy");
        return season;
    } else {
        let season = String::from("The Aftermath");
        return season;
    }
}

fn seasonnum(day: u32) -> u32 {
    if day <= 73 {
        let season = 1;
        return season;
    } else if day == 366 {
        let season = 1;
        return season;
    } else if day >= 74 && day < 147 {
        let season = 2;
        return season;
    } else if day >= 147 && day < 220 {
        let season = 3;
        return season;
    } else if day >= 220 && day < 293 {
        let season = 4;
        return season;
    } else {
        let season = 5;
        return season;
    }
}

// Determines the day of the weekday
fn weekday(day: u32) -> String {
    let wday = day % 5;
    if wday == 0 {
        let sday = String::from("Setting Orange");
        return sday;
    } else if wday == 1 {
        let sday = String::from("Sweetmorn");
        return sday;
    } else if wday == 2 {
        let sday = String::from("Boomtime");
        return sday;
    } else if wday == 3 {
        let sday = String::from("Pungenday");
        return sday;
    } else {
        let sday = String::from("Prickle-Prickle");
        return sday;
    }
}

// Determines the day of the Season
fn seasonday(day: u32) -> u32 {
    let sday = day % 73;
    if sday == 0 {
        return day;
    } else {
        return sday;
    }
}
